package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class TransactionJsonMapper
{

    protected ObjectMapper objectMapper;

    public TransactionJsonMapper()
    {
        this.setupObjectMapper();
    }

    private void setupObjectMapper()
    {
        this.objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }

    public Transaction transform(String jsonString) throws IOException
    {
        return objectMapper.readValue(jsonString, Transaction.class);
    }

}

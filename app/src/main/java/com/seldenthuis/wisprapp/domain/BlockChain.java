package com.seldenthuis.wisprapp.domain;

import java.util.ArrayList;

public class BlockChain extends ArrayList<Block>
{
    public BlockGotAddedListener blockGotAddedListener;

    public void setBlockGotAddedListener(BlockGotAddedListener blockGotAddedListener)
    {
        this.blockGotAddedListener = blockGotAddedListener;
    }

    // This class makes sure the next added block matches the previous block's prev hash.
    @Override
    public boolean add(Block block)
    {
        if (this.size() == 0) {
            return super.add(block);
        }

        Block prevBlock = this.get(this.size() - 1);
        if (prevBlock.previousBlockHash.equals(block.hash)) {
            if (this.blockGotAddedListener != null) {
                this.blockGotAddedListener.addedBlock(block);
            }
            return super.add(block);
        } else {
            return false;
        }
    }

    public Block getTip()
    {
        return this.get(0);
    }

    public Block getTail()
    {
        return this.get(this.size() - 1);
    }


    public interface BlockGotAddedListener
    {
        void addedBlock(Block block);
    }
}

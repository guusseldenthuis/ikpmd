package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Block
{
    public final String hash;

    public final int confirmations;

    public final int size;

    public final int height;

    public final String merkleroot;

    public final String[] tx;

    public final Date time;

    public final long nonce;

    public final double difficulty;

    public final String chainwork;

    public final String previousBlockHash;

    public final String nextBlockHash;

    public final int moneySupply;

    @JsonCreator
    public Block(
            @JsonProperty("hash") String hash,
            @JsonProperty("confirmations") int confirmations,
            @JsonProperty("size") int size,
            @JsonProperty("height") int height,
            @JsonProperty("merkleroot") String merkleroot,
            @JsonProperty("tx") String[] tx,
            @JsonProperty("time") long time,
            @JsonProperty("nonce") long nonce,
            @JsonProperty("difficulty") double difficulty,
            @JsonProperty("chainwork") String chainwork,
            @JsonProperty("previousblockhash") String previousBlockHash,
            @JsonProperty("nextblockhash") String nextBlockHash,
            @JsonProperty("moneysupply") int moneySupply)
    {
        this.hash = hash;
        this.confirmations = confirmations;
        this.size = size;
        this.height = height;
        this.merkleroot = merkleroot;
        this.tx = tx;
        this.time = new Date(time * 1000);
        this.nonce = nonce;
        this.difficulty = difficulty;
        this.chainwork = chainwork;
        this.previousBlockHash = previousBlockHash;
        this.nextBlockHash = nextBlockHash;
        this.moneySupply = moneySupply;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Block block = (Block) o;
        return Objects.equals(hash, block.hash);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(hash);
    }

    @Override
    public String toString()
    {
        return "Block{" +
                "hash='" + hash + '\'' +
                ", confirmations=" + confirmations +
                ", size=" + size +
                ", height=" + height +
                ", merkleroot='" + merkleroot + '\'' +
                ", tx=" + Arrays.toString(tx) +
                ", time=" + time +
                ", nonce=" + nonce +
                ", difficulty=" + difficulty +
                ", chainwork='" + chainwork + '\'' +
                ", previousBlockHash='" + previousBlockHash + '\'' +
                ", nextBlockHash='" + nextBlockHash + '\'' +
                ", moneySupply=" + moneySupply +
                '}';
    }

}

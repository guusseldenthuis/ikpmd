package com.seldenthuis.wisprapp.domain.enums;

import java.util.HashMap;
import java.util.Map;

public enum TransactionType
{
    ValueIn("vin"),
    ValueOut("vout");

    private String value;

    TransactionType(String envUrl)
    {
        this.value = envUrl;
    }

    public String getValue()
    {
        return value;
    }

    //Lookup table
    private static final Map<String, TransactionType> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (TransactionType transactionType : TransactionType.values()) {
            lookup.put(transactionType.getValue(), transactionType);
        }
    }

    //This method can be used for reverse lookup purpose
    public static TransactionType get(String transactionType)
    {
        return lookup.get(transactionType);
    }
}

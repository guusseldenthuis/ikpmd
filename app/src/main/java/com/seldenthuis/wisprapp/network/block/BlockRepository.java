package com.seldenthuis.wisprapp.network.block;

public interface BlockRepository
{

    void getByBlockHash(String address);

    void getBlockHashByIndex(int index);

    void getBlockHeight();

}

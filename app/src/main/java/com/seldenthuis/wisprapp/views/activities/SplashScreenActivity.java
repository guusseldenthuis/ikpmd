package com.seldenthuis.wisprapp.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.seldenthuis.wisprapp.R;

public class SplashScreenActivity extends Activity
{

    /**
     * Inflate the splashScreen resource.
     *
     * @param savedInstanceState Not using a saved-state right now.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    /**
     * This (lifeCycle) method launches after all initialization has been done.
     */
    @Override
    public void onStart()
    {
        super.onStart();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {

            @Override
            public void run()
            {
                // Start the main/overview activity.
                //ToDo: Maak hier een check je niet al een sessie hebt.
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }, 500);

    }

    @Override
    public void onPause()
    {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {

            @Override
            public void run()
            {
                // Start the main/overview activity.
                SplashScreenActivity.this.finish();
            }
        }, 500);
        super.onPause();
    }
}

package com.seldenthuis.wisprapp.views.adapters;

public interface AdapterPositionListener
{
    void adapterItemPosition(int position);
}

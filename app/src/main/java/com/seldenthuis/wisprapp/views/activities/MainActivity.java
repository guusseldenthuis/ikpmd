package com.seldenthuis.wisprapp.views.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.seldenthuis.wisprapp.R;
import com.seldenthuis.wisprapp.domain.Block;
import com.seldenthuis.wisprapp.views.fragments.OverviewFragment;
import com.seldenthuis.wisprapp.views.fragments.TransactionsFragment;
import com.seldenthuis.wisprapp.views.interfaces.OnBlockClickedListener;

public class MainActivity extends AppCompatActivity implements OnBlockClickedListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        this.setContentView(R.layout.activity_main);

        this.setupFragment();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

    }

    private void setupFragment()
    {
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, new OverviewFragment(), OverviewFragment.TAG)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_blockchain:
                Log.d("Seldenthuis", "onOptionsItemSelected: blockchain");
                this.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new OverviewFragment(), OverviewFragment.TAG)
                        .commit();
                return true;
            case R.id.action_stakes:
                Log.d("Seldenthuis", "onOptionsItemSelected: stakes");
                this.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new TransactionsFragment(), TransactionsFragment.TAG)
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(Block block)
    {
        TransactionsFragment transactionsFragment = new TransactionsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("blockHash", block.hash);
        transactionsFragment.setArguments(bundle);

        this.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, transactionsFragment).addToBackStack(TransactionsFragment.TAG).commit();
    }
}

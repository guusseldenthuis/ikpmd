package com.seldenthuis.wisprapp.views.presenters;

import com.seldenthuis.wisprapp.domain.Transaction;

public interface TransactionApiPresenter
{

    void transaction(Transaction transaction);

    void onError(String errorMessage);

}
